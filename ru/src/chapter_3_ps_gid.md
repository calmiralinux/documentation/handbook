# GID и EGID

GID (*Group ID*) - это идентификатор группы, к которой принадлежит владелец
процесса. EGID связан с GID так же, как и EUID с UID, т.е. они будут отличаться,
если программа запускается с установленным битом `setgid`.
