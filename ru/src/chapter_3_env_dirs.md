# Перемещение по директориям

Директории являются хорошим способом организовать свои файлы. Аналогичная вещь
есть и в MS Windows - там они называются *папками*.

Для перемещения по директориям используется команда `cd`:

```bash
cd <DIRNAME>
```

<small>Замените <tt><code>&lt;DIRNAME&gt;</code></tt> на имя директории, в
которую хотите перейти.</small>

## Файлы . и ..

Введите команду:

```bash
% ls -al
```

Ключ `-a` здесь используется для отображения скрытых файлов (файл с точкой в
начале имени считается скрытым), а ключ `-l` отображает подробную информацию о
файле. Вывод команды будет примерно таким:

```
total 48
drwxr-xr-x 4 admin admin  4096 Jan  2 22:07 .
drwxr-xr-x 4 admin admin  4096 Dec 29 17:43 ..
drwxr-xr-x 8 admin admin  4096 Jan  2 22:07 .git
-rw-r--r-- 1 admin admin     9 Dec 29 14:20 .gitignore
-rw-r--r-- 1 admin admin 20137 Dec 29 13:56 LICENSE
-rw-r--r-- 1 admin admin  4498 Dec 29 16:45 README.md
drwxr-xr-x 4 admin admin  4096 Jan  2 15:28 ru
```

Гарантирую, что в первых двух строках вы увидите два непонятных файла: `.` и
`..`. Первый файл (`.`) указывает на текущую директорию, а второй (`..`) - на
директорию выше. Например, вы находитесь в директории `/root`. Если вы выполните
команду:

```bash
% cd ..
```

То вы окажетесь в директории `/`.

## Текущее расположение

Для того, чтобы узнать, в какой директории вы сейчас находитесь, можно
воспользоваться тремя способами.

### Приглашение BASH

В начале этого руководства велась речь о приглашении к вводу. Его составляющие:

```
<USERNAME>:<WORK_DIR><LOGIN_TYPE>
```

`<USERNAME>` - это имя пользователя, от имени которого в данный момент времени
произведён вход в текущий сеанс. `<WORK_DIR>` - это текущая рабочая директория,
а `<LOGIN_TYPE>` - это символ `%` или `#`. Например:

```
admin:~%
```

Показывает, что сеанс от имени пользователя `admin`, что он находится в своей
корневой директории (`~` означает рабочую директорию пользователя в `/home`.
Например, `/home/admin`), а также `admin` - это обычный пользователь (это
показывает символ `%`).

```admonish bug title="Обратите внимание!"
В системном приглашении не показывается полный путь до директории, а только имя
текущей рабочей. К примеру, в данный момент времени мы находимся в директории
`/home/admin/calmira/handbook`, а в системном приглашении будет отображено
только `handbook`. Это сделано для того, чтобы системное приглашение не было
очень большим в случае, если длинные имена директорий или их большой уровень
вложенности.
```

### Переменная PWD

```bash
% echo $PWD
/home/admin/calmira/Docs/handbook
```

### Программа pwd

```bash
% pwd
/home/admin/calmira/Docs/handbook
```

## Создание директорий

Для того, чтобы создать директорию, используйте команду `mkdir`:

```bash
% mkdir DIRNAME
```

Если вы хотите создать кроме директории ещё и поддиректорию (или несколько
поддиректорий), то используйте ключ `-p`. К примеру, вы хотите создать новую
директорию `dir1`, а в ней директории `dir1/films` и `dir1/films/porn`. Для
этого используйте команду:

```bash
% mkdir -p dir1/films/porn
```

В итоге будут созданы 3 директории: `dir1/`, `dir1/films/` и `dir1/films/porn/`.

### Подробный вывод

Если вы хотите, чтобы `mkdir` выводил подробную информацию о своей работе,
используйте ключ `-v`:

```bash
% mkdir -pv dir1/films/porn
```

Вывод будет таким:

```
mkdir: created directory 'dir1'
mkdir: created directory 'dir1/films'
mkdir: created directory 'dir1/films/porn'
```

## Построение дерева директорий

Для того, чтобы посмотреть содержимое директории (включая содержимое
поддиректорий оттуда), используйте программу `tree` из состава порта
`general/tree`:

```bash
% tree dir1
```

Вывод будет таким:

```
dir1
├── films
│   ├── porn
│   │   └── Calmira installation guide.mp4
│   └── stallman.webm
└── Коррозия Металла - Реп это кал.mp3

3 directories, 3 files
```

Программа `tree` построит дерево со списком каталогов и файлов в них, а также
выведет количество существующих в директории файлов и поддиректорий.
