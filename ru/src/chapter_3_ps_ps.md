# Использование ps

`ps` используется для просмотра текущих выполняемых процессов. С помощью неё
можно получить информацию о процессах, PID, приоритете и прочих подобных вещах.
Кроме того, она используется для получения сведений о том, какой объём памяти
использует процесс, каково состояние процесса (выполняется, остановлен и пр.).

## Все процессы

Для получения списка всех процессов используется команда:

```bash
% ps aux
USER    PID %CPU %MEM    VSZ    RSS TTY     STAT START  TIME COMMAND
root      1  0.0  0.1   2496   1704 ?       Ss   16:54  0:00 init [3]
root      2  0.0  0.0      0      0 ?       S    16:54  0:00 [kthreadd]

  ...

admin 23732  0.0  0.3   8660   3548 tty1    R+   18:41  0:00 ps aux
admin 23733  0.0  0.2   7108   2376 tty1    S+   18:41  0:00 tail -n 2
```

```admonish tip title="Обратите внимание"
Команды, заключённые в квадратные скобки (столбец **`COMMAND`**), являются
потоками ядра, запланированными в качестве процессов.
```

Описание столбцов:

- **`USER`** - имя владельца процесса;
- **`PID`** - идентификатор процесса (Process ID);
- **`%CPU`** - доля времени центрального процессора (ЦП) в процентах под
  процесс;
- **`%MEM`** - сколько реальной памяти (в процентах) занято процессом;
- **`VSZ`** - виртуальный размер процесса;
- **`RSS`** - количество страниц памяти;
- **`TTY`** - идентификатор управляющего терминала;
- **`STAT`** - Текущий статус процесса (см. таблицы далее);
- **`TIME`** - Время ЦП, затраченное на выполнение процесса;
- **`COMMAND`** - Команда (имя программы и её аргументы).

~~~admonish bug title="Внимание"
В системе выполняется достаточно много процессов. Поэтому вывод `ps` может не
уместиться в экран. Для этого используйте программу `less` чтобы просматривать
вывод `ps` постранично - просто объедините `ps` и `less` в конвейер:

```bash
% ps aux | less
```

Для навигации используйте клавиши со стрелками, а также <kbd>Page Up</kbd> и
<kbd>Page Down</kbd>, для выхода нажмите <kbd>q</kbd>, для поиска информации в
выводе: <kbd>/</kbd> (как при просмотре документации `man`: эта программа также
использует `less` для просмотра Manual Pages).
~~~

Для просмотра всех процессов, принадлежащих определённому пользователю (по UID и
EUID), используйте команду:

```bash
% ps -U <USER> -u <USER> u
```

<small>Замените `<USER>` на имя пользователя.</small>

### Статусы процессов

Основные статусы:

| Обозначение | Описание |
|:-----------:|----------|
| `R` | Выполняется |
| `D` | Ожидает записи на диск |
| `S` | Неактивен более 20 секунд |
| `T` | Приостановлен |
| `Z` | Зомби |

Дополнительные флаги:

| Обозначение | Описание |
|:-----------:|----------|
| `W` | Процесс выгружен на диск |
| `<` | Повышенный приоритет процесса |
| `N` | Пониженный приоритет процесса |
| `L` | Часть страниц блокировано в ОЗУ |
| `s` | Процесс является лидером сеанса |

## Дополнительные опции

| Команда | Описание |
|---------|----------|
| `ps x` | Показать все процессы, запущенные текущим пользователем |
| `ps ax` | Показать все процессы                                  |
| `ps u` | Детализированная информация о процессах                 |
| `ps w` | Отображать полные названия команд                       |
