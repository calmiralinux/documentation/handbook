<h1>Calmira GNU/Linux-libre Handbook</h1>
<p><b>Автор:</b> Михаил Краснов <a
href="mailto:linuxoid85@gmail.com">&lt;linuxoid85@gmail.com&gt;</a></p>
<p><b>Версия:</b> 2023.1</p>
<p><b>Для версии Calmira:</b> 2.0 (2023.1)</p>
<p><b>Лицензия:</b> CC BY-SA</p>
<img src="pic/calmira_logo_transparent.png">
