# Приложение А - типы файлов

## Всё есть файл

Для начала стоит уяснить концепцию "всё есть файл", унаследованную из UNIX. Эта
концепция нужна для предоставления простого доступа ко всем возможностям
ОС, железу ПК и прочему, не разрабатывая отдельных API для каждого устройства и
прочих костылей. В GNU/Linux есть *корневая файловая система* (*ФС*), куда
монтируются раздел жёсткого диска с системой, другие разделы, флешки, диски и
пр.

У каждого файла есть тип. С одним типом файлов мы уже разоблались на предыдущей
странице - это *директории*. Кроме директорий есть *обычные* и *специальные*
файлы.

### Обычные файлы

С обычными файлами работают чаще всего. Для того, чтобы просмотреть эти файлы,
можно использовать команду `ls -l <DIRNAME>`. В первом столбце будут выведены
сведения о правах доступа к файлу. В этом столбце нас интересует первый символ -
если файл обычный, то этот первый символ будет `-`:

```bash
% ls -l /etc
total 1320
-rw-r--r-- 1 root root       44 Dec 19 17:56 adjtime
drwxr-xr-x 3 root root     4096 Dec 19 17:54 alsa
drwxr-xr-x 2 root root     4096 Dec  9 10:03 binfmt.d
dr-xr-xr-x 2 root root     4096 Dec 28 19:23 bluetooth
-rw-r----- 1 root brlapi     33 Dec 28 19:23 brlapi.key
-rw-r--r-- 1 root root    29874 Oct 21 21:46 brltty.conf
drwxr-xr-x 4 root root     4096 Dec 19 17:54 ca-certificates
-rw-r--r-- 1 root root      600 Dec 29 02:17 calm-release
drwxr-xr-x 2 root root     4096 Dec 28 19:23 cifs-utils
drwxr-xr-x 2 root root     4096 Dec 21 01:27 conf.d
-rw-r--r-- 1 root root      174 Dec 21 22:52 cport.conf
-rw------- 1 root root      722 Oct 19 00:01 crypttab
drwxr-xr-x 4 root cups     4096 Jan  3 13:58 cups
drwxr-xr-x 2 root root     4096 Dec 19 17:54 debuginfod
drwxr-xr-x 2 root root     4096 Dec 27 11:31 default
-rw-r--r-- 1 root root       97 Sep  9  2021 environment
-rw-r--r-- 1 root root     1362 Jul 25 17:16 ethertypes
drwxr-xr-x 3 root root     4096 Dec 19 17:54 fonts
-rw-r--r-- 1 root root      219 Dec 19 17:56 fstab
-rw-r--r-- 1 root root      694 Sep 12 19:59 fuse.conf
```

Для того, чтобы отсеять всё лишнее, используйте:

```bash
% ls -l /etc |grep "^-"
-rw-r--r-- 1 root root       44 Dec 19 17:56 adjtime
-rw-r----- 1 root brlapi     33 Dec 28 19:23 brlapi.key
-rw-r--r-- 1 root root    29874 Oct 21 21:46 brltty.conf
-rw-r--r-- 1 root root      600 Dec 29 02:17 calm-release
-rw-r--r-- 1 root root      174 Dec 21 22:52 cport.conf
-rw------- 1 root root      722 Oct 19 00:01 crypttab
-rw-r--r-- 1 root root       97 Sep  9  2021 environment
-rw-r--r-- 1 root root     1362 Jul 25 17:16 ethertypes
-rw-r--r-- 1 root root      219 Dec 19 17:56 fstab
-rw-r--r-- 1 root root      694 Sep 12 19:59 fuse.conf
```

### Специальные файлы

Эти файлы обеспечивают обмен информации с ядром, работу с устройствами и пр.
Делятся ещё на несколько видов:

- **Символьные файлы** - любые специальные файлы, например, `/dev/null`, либо
  периферийные устройства (последовательные/параллельные порты).
  Идентифицированы символом `c`.
- **Блочные** - периферийные устройства, но в отличие от предыдущего типа,
  содержимое блочных файлов буферизируется. Идентифицированы символом `b`.
- **Символические ссылки** - указывают на другие файлы по их имени, указывают и
  на другие файлы, в т.ч. каталоги. Идентифицированы символом `l`. В выводе
  команды `ls -l |grep "^l"` будет указан и файл, на который симлинк ссылается.
- **Туннели** (*каналы*/*именованные каналы*) - похожи на туннели из Shell, но
  разница в том, что именованные каналы <small>(как вы догадались?!)</small>
  имеют название. Очень редки; обозначены символом `p`.

```bash
% # Символьные файлы:
% ls -l /dev | grep "^c"
crw-rw-rw-  1 root  root     1,   3 Jan  3 13:58 null
crw-------  1 root  root    10, 144 Jan  3 13:58 nvram
crw-r-----  1 root  kmem     1,   4 Jan  3 13:58 port
crw-------  1 root  root   108,   0 Jan  3 13:58 ppp
crw-------  1 root  root    10,   1 Jan  3 13:58 psaux
crw-rw-rw-  1 root  tty      5,   0 Jan  3 13:58 tty
crw--w----  1 root  tty      4,   0 Jan  3 13:58 tty0
crw--w----  1 root  tty      4,   1 Jan  3 13:58 tty1
crw--w----  1 root  tty      4,  10 Jan  3 13:58 tty10
crw--w----  1 root  tty      4,  11 Jan  3 13:58 tty11
crw--w----  1 root  tty      4,  12 Jan  3 13:58 tty12
crw--w----  1 root  tty      4,  13 Jan  3 13:58 tty13
crw--w----  1 root  tty      4,  14 Jan  3 13:58 tty14
crw--w----  1 root  tty      4,  15 Jan  3 13:58 tty15
```

```bash
% # Блочные файлы
% ls -l /dev |grep "^b"
brw-rw----  1 root  disk     8,   0 Jan  3 13:58 sda
brw-rw----  1 root  disk     8,   1 Jan  3 13:58 sda1
brw-rw----  1 root  disk     8,   2 Jan  3 13:58 sda2
brw-rw----  1 root  disk     8,   3 Jan  3 13:58 sda3
brw-rw----  1 root  disk     8,   4 Jan  3 13:58 sda4
brw-rw----  1 root  disk     8,   5 Jan  3 13:58 sda5
brw-rw----  1 root  disk     8,   6 Jan  3 13:58 sda6
brw-rw----  1 root  disk     8,  16 Jan  3 13:58 sdb
brw-rw----  1 root  disk     8,  17 Jan  3 13:58 sdb1
brw-rw----  1 root  disk     8,  32 Jan  3 14:31 sdc
brw-rw----  1 root  disk     8,  33 Jan  3 14:31 sdc1
brw-rw----  1 root  disk     8,  34 Jan  3 14:31 sdc2
brw-rw----  1 root  disk     8,  48 Jan  3 14:51 sdd
```

```bash
% # Символические ссылки (симлинки)
% ls -l /dev | grep "^l"
lrwxrwxrwx  1 root  root     11 Jan  3 13:58 core -> /proc/kcore
lrwxrwxrwx  1 root  root     13 Jan  3 13:58 fd -> /proc/self/fd
lrwxrwxrwx  1 root  root      4 Jan  3 13:58 rtc -> rtc0
lrwxrwxrwx  1 root  root     15 Jan  3 13:58 stderr -> /proc/self/fd/2
lrwxrwxrwx  1 root  root     15 Jan  3 13:58 stdin -> /proc/self/fd/0
lrwxrwxrwx  1 root  root     15 Jan  3 13:58 stdout -> /proc/self/fd/1
```

```bash
% # БОНУС - директории обозначены символом `d`:
% ls -l /dev |grep "^d"
drwxr-xr-x  2 root  root        300 Jan  3 14:51 block
drwxr-xr-x  2 root  root        120 Jan  3 14:51 bsg
drwxr-xr-x  3 root  root         60 Jan  3 13:58 bus
drwxr-xr-x  2 root  root       4500 Jan  3 14:51 char
```

### Узнать тип файла

Есть программа `file` (в Calmira GNU/Linux-libre предустановлена по умолчанию;
находится в порту `base/file`), которая предназначена для определения типа
файла:

```bash
% file /etc/fstab
/etc/fstab: ASCII text
% file /dev/sda
/dev/sda: block special (8/0)
% file /bin/cpkg
/bin/cpkg: LF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=e9b990f12f261c09d9ca47f46531406f677016d7, for GNU/Linux 4.4.0, stripped
```
