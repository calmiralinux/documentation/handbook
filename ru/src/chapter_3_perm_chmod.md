# Стандартное управление доступом

Из [предыдущего пункта](chapter_3_perm_intro.md) мы узнали, что у пользователей
есть 3 основных права доступа к файлам: чтение, исполнение и запись. Эти права
распространяются на 3 сущности: на владельца файла, на группу, в которую входит
владелец файла, а также на всех остальных пользователей.

Пользователь, создавший файл, становится его владельцем, ровно как и группа, в
которую входит этот пользователь и, возможно, некоторые другие пользователи.

## Изменение владельца файла

Для того, чтобы изменить владельца файла, используется программа `chown`.
Синтаксис у этой программы следующий:

```bash
chown ПОЛЬЗОВАТЕЛЬ [ОПЦИОНАЛЬНЫЕ КЛЮЧИ] /ПУТЬ/К/ФАЙЛУ
```

### Ключи chown

- `-R`, `--recursive` - рекурсивная обработка всех подкаталогов.
- `--dereference` - изменять права для файла, к которому ведёт символическая
  ссылка, вместо самой ссылки (*по умолчанию*).
- `--no-dereference`, `-h` - изменять права символических ссылок, но оставить
  неизменными файлы, на которые указывают эти ссылки.

<!-- TODO: добавить примеры! -->

## Изменение группы файла

Для изменения группы файла используется программа `chgrp`. В отличие от
предыдущей программы `chown`, для `chgrp` требуется только имя группы, имя
пользователя не нужно.

Синтаксис:

```bash
chgrp [ОПЦИОНАЛЬНЫЕ КЛЮЧИ] ИМЯ_ГРУППЫ /ПУТЬ/К/ДИРЕКТОРИИ
```

### Ключи и опции chgrp

- `--no-dereference`, `-h` - работать с символьными ссылками, а не файлами, на
  которые они указывают.
- `-R` - рекурсивно изменить группы для каталогов и их содержимого, а
  возникающие ошибки не прекратят исполнение программы.

<!-- TODO: добавить ещё несколько ключей -->

<!-- TODO: добавить примеры! -->
