# Основные программы

![](./pic/chapter_3_env_bash.png)

## BASH [^1]

В качестве основной командной оболочки в Calmira GNU/Linux-libre используется
BASH (**B**ourne **a**gain **sh**ell) - одна из наиболее функциональных и
популярных разновидностей командной оболочки UNIX.

Работает в интерактивном режиме, однако умеет читать команды из файла,
называемым *скриптом* или *сценарием*. Поддерживает автодополнение (по нажатию
клавиши <kbd>Tab</kbd>), переменные, ветвление и циклы. Кроме того, есть история
команд (по команде `history`).

[^1]: подробная информация о BASH будет далее.

## Утилиты для работы с текстовой информацией

Вы уже познакомились с текстовым редактором NeoVim в [части
2](chapter_2_editors.md). Однако, помимо `nvim`, в Calmira GNU/Linux-libre есть
потоковый текстовый редактор (ПТР) `sed`. Это не только ПТР, но и Тьюринг-полный
язык программирования ([ссылка
1](https://web.archive.org/web/20180220011912/http://sed.sourceforge.net/grabbag/scripts/turing.txt),
[ссылка
2](https://web.archive.org/web/20180116201401/http://sed.sourceforge.net/grabbag/scripts/turing.sed)).

Другое применение `sed` - фильтр. Помогает отфильтровать какие-либо ненужные
данные, выведя только необходимую информацию. Либо заменить какой-либо текст на
другой. Часто используется в конвейерах (пайпах):

```bash
% neofetch --ascii_distro Arch | sed -e 's/Ubuntu 20.04/Arch Linux/'
```

Кроме `sed` есть ещё один инструмент - `awk` (GNU AWK в Calmira
GNU/Linux-libre). Это С-подобный сценарный (скриптовый) язык построчного разбора
и обработки входного потока. Например:

Печать самой длинной строки:

```awk
{ if (length($0) > max) max = length($0) }
END { print max }
```

`grep` (search **g**lobally for lines matching the **r**egular **e**xpression,
and **p**rint them)- программа для поиска строки, отвечабщей заданному
регулярному выражению.
