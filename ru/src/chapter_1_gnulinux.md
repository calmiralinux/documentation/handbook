# Что такое GNU/Linux?

## Операционные системы семейства UNIX

Традиционно, операционная система UNIX состоит из следующих компонентов: ядро,
загрузчик, пользовательская оболочка, утилиты для работы с файлами,
пользователями, процессами и т.д. Впоследствии такая модульная огранизация ОС,
где отдельный компонент системы делает какое-то одно конкретное действие,
перенеслась и на другие системы, в том числе ОС, основанные или являющиеся
подобными изначальной UNIX. К этим системам относится и GNU/Linux.

Строение операционных систем семейства UNIX более-менее стандартизировано. От
системы к системе примерно одинаковая структура директорий и системные файлы,
соответственно, находятся в привычных местах. Набор утилит также +-
стандартизирован. У нас есть ядро, загрузчик, система инициализации, утилиты
для взаимодействием с ОС (управление пользователями, процессами, правами доступа
и т.д.), для работы с файлами и каталогами и т.д. Некоторые программы
объединяются в целые пакеты, например, пакет `coreutils` содержит в себе
программы для работы с файлами, управления процессами и пользователями. И у нас
есть несколько реализаций этого пакета. Например, GNU Coreutils и uutils
coreutils. Каждая из реализаций находится на своём определённом уровне развития,
какие-то развиты и полноценны, а какие-то всё ещё находятся в состоянии
разработки.

## Почему GNU/Linux?

Операционная система состоит из множества компонентов, среди которых особенно
выделяются Linux и ПО от проекта GNU. Linux является ядром, оно предоставляет
базовые абстракции для работы с файлами (информация на жёстких дисках - это
просто поток нулей и единиц, а драйвер файловой системы, являющийся компонентом
ядра, предоставляет пользователю простую абстракцию в виде файлов с определённым
именем, расположением и правами доступа), процессами (т.е. управляет памятью
компьютера так, чтобы каждому процессу хватало этой памяти, и чтобы каждый
процесс был как бы изолирован друг от друга во избежание конфликтов и ошибок в
работе), пользователями и пр. Для программистов ядро предоставляет набор функций
для взаимодействия, предположим, с железом ПК - опять же, взаимодействие не
напрямую, а используя абстракции. К примеру спецификация стандарта SATA 3
простирается на 874 страницы. Если разработчику нужно было бы изучать его для
того, чтобы взаимодействовать с жёсткими дисками или SSD этого стандарта, то он
бы не написал ни строчки кода. Операционная система (в частности, ядро ОС)
упрощает этот процесс - программисту не требуется теперь думать об особенностях
каждого из устройств. Ему доступны операции для чтения и записи информации в
какой-либо файл или куда-то ещё - и этого вполне достаточно.

Однако ядро Linux не предоставляет пользователю многих других вещей, таких, как
командная оболочка, утилиты для работы с файлами, утилиты для управления правами
доступа к файлам, утилиты для управления процессами, пользователями,
загрузчик системы, текстовый редактор и многое другое. Всё это ПО, необходимое
для того, чтобы у нас получилась полноценная и пригодная к использованию
операционная система, было разработано в рамках проекта GNU. Именно поэтому ядро
следует называть Linux, а уже операционную систему - GNU/Linux, поскольку ОС
состоит не только из одного единственного ядра, но ещё и из многих других
компонентов, обеспечивающих работу этой ОС.

Конечно, мы можем не использовать программное обеспечение от проекта GNU для
построения операционной системы. Мы можем заменить всё это ПО на соответствующие
реализации от других разработчиков. Но тогда и операционная система будет
называться не "GNU/Linux", а как-то по-другому. Например, несмотря на то, что ОС
Android включает в себя ядро Linux, но все остальные системные компоненты там
свои, поэтому эту ОС мы называем именно Android, а не как-то по-другому.

## Свободное программное обеспечение

Операционная система GNU/Linux является *свободным ПО*. ПО можно считать
свободным, если оно гарантирует следующие вещи:

1. Свобода изучать строение ПО, редактировать исходный код ПО.
2. Свобода распространять копии ПО.
3. Свобода распространять модифицированные копии ПО.

Как правило, СПО распространяется под лицензией GNU General Public License
(GPL), которая не просто гарантирует свободу, но и защищает её. Она допускает
распространение программ только под той же лицензией, поэтому исходный код
компонентов GNU/Linux не может быть использован для создания несвободного ПО.

## Распространение GNU/Linux

Операционную систему GNU/Linux в "чистом" виде вы не увидите никогда - она
распространяется в виде "дистрибутивов". Дистрибутив - это форма распространения
программного обеспечения. Конкретно дистрибутив GNU/Linux включает в себя как
саму ОС GNU/Linux, так и множество других дополнительных компонентов: программ,
библиотек, настроек и т.д. Calmira GNU/Linux-libre - один из таких
дистрибутивов. Распространение ОС в виде готовых дистрибутивов - отличное
решение для пользователей, ведь можно подобрать дистрибутив согласно желаниям и
требованиям.

## Новичку

GNU/Linux - это не Windows, не MacOS и не какие-либо мобильные ОС вроде Android,
iOS и прочих. В GNU/Linux свои правила, их нужно изучить, к ним нужно
привыкнуть. Не обязательно, что в GNU/Linux какие-то вещи будут работать так же,
как и в вашей предыдущей ОС (а мы уверены, что Calmira GNU/Linux-libre, либо
какие-то другие дистрибутивы - это не первая для вас ОС). Терпение и
настойчивость в изучении другой ОС обернётся значительным повышением
эффективности и безопасности вашей работы. То, что сегодня кажется страшным и
непонятным, через какое-то время станет нормой и обыденностью.

Перед тем, как начать использовать тот или иной дистрибутив GNU/Linux, следует
ознакомиться как с достоинствами и недостатками GNU/Linux в целом, так и с
достоинствами и недостатками конкретного дистрибутива этой ОС для того, чтобы
определить, подходит ли вам эта система. Учитывайте ещё и то, что у каждого
дистрибутива GNU/Linux может быть своё особое предназначение. Один дистрибутив
предназначен для обычных пользователей, другой хорошо себя показывает на
серверном оборудовании, третий является конструктором для опытного пользователя.
Выберите ту систему, которая подходит именно под ваши требования. Тогда процесс
изучения GNU/Linux будет несколько проще и приятнее.

В процессе изучения GNU/Linux у вас могут возникать проблемы, поэтому не
стесняйтесь задавать вопросы на форумах, в чатах (у нас есть [Telegram
чат](https://t.me/calmira_gnu_linux_chat), кстати), искать нужную информацию в
интернете. В сообществе свободного ПО сложилась традиция взаимопомощи, да и
просто общения между собой.
