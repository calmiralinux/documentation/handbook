# PID и PPID

## PID

Ядро назначает каждому процессу уникальный идентификатор. Большинство команд и
системных вызовов, работающих с процессами, требуют конкретного указания
идентификатора. Такие идентификаторы называются "*Process ID*" (PID). PID
уникален для каждого процесса (разве что система инициализации должна иметь PID
= 1).

## PPID

В UNIX-системах нет системного вызова для создания нового процесса. Для создания
нового процесса существующий должен клонировать сам себя. Клон может заменить
выполняемую программу другой.

В операции клонирования исходный процесс называется *родительским*, а его клон -
*дочерним*. Каждый дочерний процесс помимо PID имеет атрибут PPID (*Parent
Process ID*), который совпадает с идентификатором породившего его родительского
процесса. Но из этого правила есть исключение - если родительский процесс по
какой-либо причине завершается раньше потомка, система инициализации подставляет
свой PID на место предка.
